//
//  conversationsManager.swift
//  conversationsManager
//
//  Created by Jeffrey Linwood on 3/11/20.
//  Copyright © 2020 Twilio, Inc. All rights reserved.
//

import UIKit
//import TwilioChatClient
import TwilioConversationsClient

protocol QuickstartConversationsManagerDelegate: AnyObject {
    func reloadMessages()
    func receivedNewMessage(message : TCHMessage)
    func displayStatusMessage(_ statusMessage:String)
    func displayErrorMessage(_ errorMessage:String)
}

class QuickstartConversationsManager: NSObject, TwilioConversationsClientDelegate {

    // the unique name of the channel you create
    private let uniqueChannelName = "general"
    private let friendlyChannelName = "General Channel"

    static let shared = QuickstartConversationsManager()
    // For the quickstart, this will be the view controller
    weak var delegate: QuickstartConversationsManagerDelegate?

    // MARK: Chat variables
    public var client: TwilioConversationsClient?
//    private var conversation: TCHConversation?
    private(set) var messages: [TCHMessage] = []
//    private var identity: String?
    

    func conversationsClient(_ client: TwilioConversationsClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
        guard status == .completed else {
            return
        }
//        checkConversationCreation { (_, conversation) in
//            if let conversation = conversation {
//                self.joinConversation(conversation)
//            } else {
//                print("not conversation")
//            }
//        }
    }

    // Called whenever a channel we've joined receives a new message
    func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation,
                             messageAdded message: TCHMessage) {
        if message.hasMedia() {
            message.getMediaContentTemporaryUrl { (result, mediaContentUrl) in
                guard let mediaContentUrl = mediaContentUrl else {
                    return
                }
                // Use the url to download an image or other media
//                print(mediaContentUrl)
            }
            
        }
        messages.append(message)
        
        // Changes to the delegate should occur on the UI thread
        DispatchQueue.main.async {
            if let delegate = self.delegate {
                delegate.receivedNewMessage(message: message)
                delegate.reloadMessages()

            }
        }
    }

    func sendMessage(_ conversation : TCHConversation ,_ messageText: String,
                         completion: @escaping (TCHResult, TCHMessage?) -> Void) {
            
            let messageOptions = TCHMessageOptions().withBody(messageText)
            conversation.sendMessage(with: messageOptions, completion: { (result, message) in
                completion(result, message)
            })
        
        }
    func sendMediaMessage(_ conversation : TCHConversation,_ inputStream: InputStream,
                         completion: @escaping (TCHResult, TCHMessage?) -> Void) {
        let messageOptions = TCHMessageOptions().withMediaStream(inputStream, contentType: "image/jpeg",defaultFilename: "image.jpg", onStarted: {
                                        // Called when upload of media begins.
            print("Media upload started")
        },
        onProgress: { (bytes) in
            // Called as upload progresses, with the current byte count.
            print("Media upload progress: \(bytes)")
        }) { (mediaSid) in
            // Called when upload is completed, with the new mediaSid if successful.
            // Full failure details will be provided through sendMessage's completion.
            print("Media upload completed")
        }
        conversation.sendMessage(with: messageOptions, completion: { (result, message) in
            completion(result, message)
        })
    
    }
    func sendMediaMessageVideo(_ conversation : TCHConversation, _ inputStream: InputStream,
                         completion: @escaping (TCHResult, TCHMessage?) -> Void) {
        let messageOptions = TCHMessageOptions().withMediaStream(inputStream, contentType: "video/mp4",defaultFilename: "video.mp4", onStarted: {
                                        // Called when upload of media begins.
            print("Media upload started")
        },
        onProgress: { (bytes) in
            // Called as upload progresses, with the current byte count.
            print("Media upload progress: \(bytes)")
        }) { (mediaSid) in
            // Called when upload is completed, with the new mediaSid if successful.
            // Full failure details will be provided through sendMessage's completion.
            print("Media upload completed")
        }
        conversation.sendMessage(with: messageOptions, completion: { (result, message) in
            completion(result, message)
        })
    
    }
    
    func login( token: String, completion: @escaping (Bool) -> Void) {
        print(token)
        print("Client: ",self.client)

//        if self.client == nil{
            TwilioConversationsClient.conversationsClient(withToken: token,
                                                          properties: nil,
                                                          delegate: self) { (result, client) in
                self.client = client
                self.client?.register(withNotificationToken:appManager.DeviceToken) { (result) in
                    if (!result.isSuccessful) {
                        print("Device Token: not registe isSuccessful")
                        // try registration again or verify token
                    }
                    else
                    {
                        print("Register Sucssesful")
                    }
                }
                completion(result.isSuccessful)
            }
//        }else{
//            completion(true)
//
//        }

//        TwilioChatClient.chatClient(withToken: token, properties: nil,
//                                        delegate: self) { (result, chatClient) in
//                print(chatClient)
//                    self.client = chatClient
//                    completion(result.isSuccessful())
//                }
    }

    func shutdown() {
        if let client = client {
//            client.delegate = nil
//            client.shutdown()
//            self.client = nil
        }
    }

    public func checkConversationCreation(conversation_sid: String,_ completion: @escaping(TCHResult?, TCHConversation?) -> Void) {
        guard let client = client else {
            return
        }
        print("IDENTITY :",identity)
        print("SID :",sid)
            client.conversation(withSidOrUniqueName: conversation_sid) { (result, conversation) in
                completion(result, conversation)
            }
    }

    public func joinConversation(_ conversation: TCHConversation) {
//        self.conversation = conversation
        print("CONVERSATION:",conversation)
        if conversation.status == .joined {
            print("Current user already exists in conversation")
//            registerToken(fcmToken: Defaults[\.fcmToken])
            self.loadPreviousMessages(conversation)
        } else {
            conversation.join(completion: { result in
                print("Result of conversation join: \(result.resultText ?? "No Result")")
                if result.isSuccessful {
                    self.loadPreviousMessages(conversation)
                }
            })
        }
    }
    func registerToken(fcmToken:String){
        client!.register(withNotificationToken: Data(fcmToken.utf8)) { (result) in
            print(result.isSuccessful)
            print(result.error)
        }
    }
    func unRegisterToken(fcmToken:String){
        print(client)
        if client != nil{
            client!.deregister(withNotificationToken: Data(fcmToken.utf8)) { (result) in
                print(result.isSuccessful)
                print(result.error)
            }
        }else{
            print("client Null")
        }
    }
    func conversationsClientTokenWillExpire(_ client: TwilioConversationsClient) {
        print("Access token will expire.")
        AppManager.getToken {
        }
    }
    func conversationsClientTokenExpired(_ client: TwilioConversationsClient) {
        print("Access token expired.")
        AppManager.getToken {
            
        }
    }

    public func loadPreviousMessages(_ conversation: TCHConversation) {
        print(conversation)
            conversation.getLastMessages(withCount: 200) { (result, messages) in
                print("MSG",messages)
                if let messages = messages {
                    self.messages = messages
                    DispatchQueue.main.async {
                        self.delegate?.reloadMessages()
                    }
                }
            }
        }
}



struct PlatformUtils {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}

struct TokenUtils {

    static func retrieveToken(url: String, completion: @escaping (String?, String?, Error?) -> Void) {
        if let requestURL = URL(string: url) {
            let session = URLSession(configuration: URLSessionConfiguration.default)
            let task = session.dataTask(with: requestURL, completionHandler: { (data, _, error) in
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        if let tokenData = json as? [String: String] {
                            let token = tokenData["token"]
                            let identity = tokenData["identity"]
                            completion(token, identity, error)
                        } else {
                            completion(nil, nil, nil)
                        }
                    } catch let error as NSError {
                        completion(nil, nil, error)
                    }
                } else {
                    completion(nil, nil, error)
                }
            })
            task.resume()
        }
    }
}
