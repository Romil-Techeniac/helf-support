//
//  NetworkClient.swift
//  Base_App
//
//  Created by Romil Dhanani on 23/05/17.
//  Copyright © 2017 Romil Dhanani. All rights reserved.
//

import Alamofire
import ObjectMapper
import NVActivityIndicatorView

public typealias ResponseData = [String: Any]
public typealias ResponseArray = [Any]
public typealias FailureMessage = String
public typealias ResponseString = String
public typealias FailureCode    = String

struct Certificates {
//    static let snapacleanerCert =
//        Certificates.certificate(filename: "marketdiamond.com")
    
//    private static func certificate(filename: String) -> SecCertificate {
//        let filePath = Bundle.main.path(forResource: filename, ofType: "der")!
//        let data = try! Data(contentsOf: URL(fileURLWithPath: filePath))
//        let certificate = SecCertificateCreateWithData(nil, data as CFData)!
//
//        return certificate
//    }
}


class NetworkClient {
    
    // MARK: - Constant
    struct Constants {
        
        struct RequestHeaderKey {
            static let contentType                  = "Content-Type"
            static let applicationJSON              = "application/json"
        }
        
        struct HeaderKey {
            
            static let Authorization                = "Authorization"
        }
        
        struct ResponseKey {
            
            static let code                         = "code"
            static let data                         = "data"
            static let message                      = "message"
            static let list                         = "list"
            static let serverLastSync               = "server_last_sync"
        }
        
        //MARK:  ResponseCode
        struct ResponseCode {
            
            static let EmailRequired                     = "EMAIL_IS_REQUIRED"
            static let DobRequired                       = "DOB_IS_REQUIRED"
            static let EmailDobRequired                  = "EMAIL_DOB_IS_REQUIRED"
            static let ok                                = "OK"
            static let kTokenExpiredCode                 = "E_TOKEN_EXPIRED"
            static let kUnAuthorized                     = "E_UNAUTHORIZED"
            static let kListNotFound                     = "E_NOT_FOUND"
            static let kNoInternet                       = "NO_INTERNET"
            static let kBadAccess                        = "E_BAD_REQUEST"
        }
    }
    
    // MARK: - Properties
    
    // A Singleton instance
    static let sharedInstance = NetworkClient()
    
    // A network reachability instance
    let networkReachability = NetworkReachabilityManager()
    
    
//    static let evaluators = [
//        "tradeapi.market.diamonds":
//            PinnedCertificatesTrustEvaluator(certificates: [
//                Certificates.snapacleanerCert
//            ])
//    ]
    
    var session =  Session()
    
    // Initialize
    private init() {
        
        networkReachability?.startListening(onUpdatePerforming: { (status) in
            print(status)
        })
        
        setIndicatorViewDefaults()
        
        session = Session(
//            serverTrustManager: ServerTrustManager(evaluators: NetworkClient.evaluators)
        )
    }
    
    // MARK: - Indicator view
    private func setIndicatorViewDefaults() {
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballSpinFadeLoader
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 40, height: 40)
        NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE_FONT = UIFont.boldSystemFont(ofSize: 17)
    }
    
    func isNetworkAvailable() -> Bool {
        
        guard (networkReachability?.isReachable)! else {
            return false
        }
        
        return true
    }
    
    /// show indicator before network request
    func showIndicator(_ message:String?, stopAfter: Double) {
        
        let activityData = ActivityData(message: message)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        if stopAfter > 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + stopAfter) {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        }
        
        //KMActivityindicator.startAnimating()
    }
    
    ///Stop Indicator Manually
    func stopIndicator() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    
    // MARK: - Network Request
    
    // Global function to call web service
    func request(_ url: URLConvertible, command: String, method: Alamofire.HTTPMethod = .get, parameters: Parameters? = nil, headers: HTTPHeaders? = nil, success:@escaping ((Any, String)->Void), failure:@escaping ((FailureMessage,FailureCode)->Void)) {
        
        // check network reachability
        guard (networkReachability?.isReachable)! else {
            
            failure("No Internet Connection.", Constants.ResponseCode.kNoInternet)
            return
        }
        
        // create final url
        let finalURLString: String = "\(url)\(command)"
        let finalURL = NSURL(string : finalURLString)! as URL
        
        print(finalURL)
        
        // Network request
        session.request(finalURL, method: method, parameters: parameters, headers: headers).responseJSON { (response: AFDataResponse<Any>) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            switch response.result {
            case .success(let value):
                
                let responseObject = value as? [String: Any]
                
                // get status code
                let statusCode = responseObject?[Constants.ResponseKey.code] as? String ?? ""
                
                // get status message
                let message = responseObject?[Constants.ResponseKey.message] as? String ?? ""
                
                //User LogOut
                if statusCode == Constants.ResponseCode.kTokenExpiredCode {
                    self.moveToLogin()
                    failure(message,statusCode)
                    return
                }
                
                //User LogOut
                if statusCode == Constants.ResponseCode.kUnAuthorized {
                    self.moveToLogin()
                    return
                }
                
                //Chek Code
//                if statusCode != Constants.ResponseCode.ok {
//                    failure(message,statusCode)
//                    return
//                }
                
                // get data object
                let dataObject = responseObject?[Constants.ResponseKey.data]
                
                // pass data as dictionary if data key contains json object
                if let data = dataObject as? ResponseData {
                    success(data, message)
                    return
                }
                else if let data = dataObject as? ResponseArray {
                    success(data, message)
                    return
                }else{
                    failure(message,statusCode)
                    return
                }
                
                
            case .failure(let error):
                let isServerTrustEvaluationError =
                    error.asAFError?.isServerTrustEvaluationError ?? false
                var message: String
                if isServerTrustEvaluationError {
                    message = "Certificate Pinning Error"
                } else {
                    message = error.localizedDescription
                }
                message = error.localizedDescription
                // check result is success
                failure(message,"")
                return
            }
        }
        
    }
    
    //Stop response Indicator
    func stopResponseIndicator(command : String) {
        
    }
    func moveToLogin(){
//        let vc = SplashVC.init(nibName: SplashVC.IDENTIFIER, bundle: nil)
        UIApplication.shared.topMostViewController()?.navigationController?.popToRootViewController(animated: true)
    }
    
}
extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
}
extension UIViewController {

        func topMostViewController() -> UIViewController {
           
            if let presented = self.presentedViewController {
                return presented.topMostViewController()
            }
           
            if let navigation = self as? UINavigationController {
                return navigation.visibleViewController?.topMostViewController() ?? navigation
            }
           
            if let tab = self as? UITabBarController {
                return tab.selectedViewController?.topMostViewController() ?? tab
            }
           
            return self
        }
}
