//
//  DefaultKeys.swift
//  Helf
//
//  Created by Techeniac Services on 18/02/21.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
    var isLoggedIn: DefaultsKey<Bool> { .init("IS_LOGGED_IN", defaultValue: false) }
    var isNotifocation: DefaultsKey<Bool> { .init("IS_NOTIFICATION", defaultValue: true) }
    var accessToken: DefaultsKey<String> { .init("ACCESS_TOKEN", defaultValue: "") }
    var fcmToken: DefaultsKey<String> { .init("FCM_TOKEN", defaultValue: "") }
}
