//
//  DateUtilities.swift
//  PTE
//
//  Created by Romil on 26/09/20.
//  Copyright © 2017  Romil. All rights reserved.
//

import UIKit

class DateUtilities: NSObject {
    
    //MARK: Constant
    
    static var formatter: DateFormatter { return DateFormatter() }
    
    struct DateFormates {
        static let kLongDate = "dd MMM yyyy, h:mm a"
        static let kOnlyTime = "hh:mm a"
        static let kRegularDate = "dd-MM-yyyy"
        static let kResultDate = "d \nMM-yyyy"
        static let kEventShortDate = "dd MMM yy"
        static let kQuesDate = "dd MMM, yyyy"
        static let kCreateCaseDate = "dd/MM/yyyy"
        static let kIsoFormate = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        static let kAnnouncementShort = "dd\nMMM"
        static let kAnnouncementLong = "dd MMM, yy"
        static let kEventDate = "dd MMMM"
        
        
    }
    
    static func dateFromTimeStamp(timestamp:Double) -> Date?{
        return Date(timeIntervalSince1970: timestamp / 1000)
    }
    
    static func convertDateFromString(dateStr: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //this your string date format
        let date = dateFormatter.date(from: dateStr)
        return date ?? Date()
    }
    
    static func convertDateFromStringWithFormat(dateStr: String, format : String = DateFormates.kRegularDate) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format //this your string date format
        let date = dateFormatter.date(from: dateStr)
        return date!
    }
    
    static func convertDateToString(date: Date?, formate:String = DateUtilities.DateFormates.kRegularDate) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        if let dt = date{
            return dateFormatter.string(from: dt)
        }else{
            return ""
        }
    }
    
    static func convertStringDateintoStringWithFormat(dateStr : String, format : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let myDate = dateFormatter.date(from: dateStr)
        
        if myDate != nil {
            dateFormatter.dateFormat = format
            //        dateFormatter.timeZone = TimeZone.current
            let somedateString = dateFormatter.string(from: myDate!)
            return somedateString
        }
        return ""
    }
    
    static func secondsFromDate(date: Date) -> Int {
        
        let current = Date()
        let seconds = date.timeIntervalSince(current)
        return Int(seconds)
    }
    
    static func dayNoFromDate(dateStr: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let myDate = dateFormatter.date(from: dateStr)
        
        if myDate != nil {
            dateFormatter.dateFormat = "d"
            let somedateString = dateFormatter.string(from: myDate!)
            return somedateString
        }
        return ""
    }
    
    static func MonthYearFromDate(dateStr: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let myDate = dateFormatter.date(from: dateStr)
        
        if myDate != nil {
            dateFormatter.dateFormat = "MMM''yy"
            let somedateString = dateFormatter.string(from: myDate!)
            return somedateString
        }
        return ""
    }
    
    static func getStringDateFromISOFormat(dateStr: String,resultFormat:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let myDate = dateFormatter.date(from: dateStr)
        
        if myDate != nil {
            dateFormatter.dateFormat = resultFormat
            let somedateString = dateFormatter.string(from: myDate!)
            return somedateString
        }
        return ""
    }
    
    static func getDateFromISO(dateStr: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: dateStr)
        
    }
    
    static func convertStringFromDate(date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateStr = dateFormatter.string(from: date)
        return dateStr
    }
    
    static func convertStringfromFormatedDate(date: Date, formate:String = DateUtilities.DateFormates.kRegularDate) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        return dateFormatter.string(from: date)
    }
    
    static func getIsoFormateCurrentDate() -> String? {
        let myDate = Date()
        let dateFormatter = DateFormatter()
        let calendar = Calendar(identifier: .gregorian)
        dateFormatter.calendar = calendar
        let locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.locale = locale as Locale
        let timeZone = NSTimeZone(forSecondsFromGMT: 0)
        dateFormatter.timeZone = timeZone as TimeZone
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"
        let myDateString = dateFormatter.string(from: myDate)
        return myDateString
    }
    
    static func getTimestampFromDate(date:Date) -> Int {
        
        // convert Date to TimeInterval (typealias for Double)
        let timeInterval = date.timeIntervalSince1970
        
        // convert to Integer
        return Int(timeInterval)
    }
 
    static func getFomattedDate(date:Date?) -> String{
        
        if date == nil{
            return ""
        }
        
        let diff = Calendar.current.dateComponents([.second], from: date!, to: Date()).second ?? 0
        
        if (diff <= 60) {
            
            return "just now"
        }
        else if (diff <= 120) {
            
            return "a minute ago"
        }
        else if (diff <= 3600) {
            
            let mnt = (diff % 3600) / 60
            return "\(mnt) minute ago"
        }
        else if (diff <= 3600) {
            
            return "an hour ago"
        }
        else if (diff <= 24 * 3600) {
            
            let hr = diff / 3600
            return "\(hr) hour ago"
        }
        else if (diff <= 48 * 3600) {
            
            return "yesterday"
        }
        else if (diff <= 7 * 86400) {
            
            let days = diff / 86400
            return "\(days) days ago"
        }
        
        return ""
    }
    
    static func daysBetweenDates(startDate: Date, endDate: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day ?? 0
    }
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date? {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)
    }
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
    
//    func getCurrentYear() -> Int {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy"
//        let strYear = dateFormatter.string(from: self)
//        
//        if let year = Int(strYear) {
//            return year
//        }
//        
//        return AppConstants.Profile.ToYearOfFellowship
//    }
    
    static func getDateListBetweenTwoDate(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        
        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }

}
extension String {
    func toDate(withFormat format: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")-> Date?{
           let dateFormatter = DateFormatter()
          dateFormatter.timeZone =  NSTimeZone(name: "UTC") as TimeZone?
          dateFormatter.calendar = Calendar(identifier: .gregorian)
          dateFormatter.dateFormat = format
          let date = dateFormatter.date(from: self)
          return date
      }
    func toDateISO(withFormat format: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")-> Date?{
           let dateFormatter = DateFormatter()
          dateFormatter.timeZone =  NSTimeZone(name: "IOS") as TimeZone?
          dateFormatter.calendar = Calendar(identifier: .gregorian)
          dateFormatter.dateFormat = format
          let date = dateFormatter.date(from: self)
          return date
      }
        subscript(i: Int) -> String {
            return String(self[index(startIndex, offsetBy: i)])
        }
    var isValidEmail: Bool {
          let regularExpressionForEmail = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
          let testEmail = NSPredicate(format:"SELF MATCHES %@", regularExpressionForEmail)
          return testEmail.evaluate(with: self)
       }
       var isValidPhone: Bool {
          let regularExpressionForPhone = "^\\d{10}$"
          let testPhone = NSPredicate(format:"SELF MATCHES %@", regularExpressionForPhone)
          return testPhone.evaluate(with: self)
       }
}
