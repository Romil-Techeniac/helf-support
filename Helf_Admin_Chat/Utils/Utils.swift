//
//  Utils.swift
//  Helf
//
//  Created by Techeniac Services on 08/02/21.
//

import Foundation
import UIKit
import AVFoundation

extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }

        return nil
    }
}

class Utils {

    static func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)

        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }

        return nil
//        guard let data = try? Data(contentsOf: url),
//          let page = PDFDocument(data: data)?.page(at: 0) else {
//            return nil
//          }
//
//          let pageSize = page.bounds(for: .mediaBox)
////              let pdfScale = width / pageSize.width
//
//          // Apply if you're displaying the thumbnail on screen
////              let scale = UIScreen.main.scale * pdfScale
//          let screenSize = CGSize(width: pageSize.width ,
//                                  height: pageSize.height)
//
//          return page.thumbnail(of: screenSize, for: .mediaBox)
    }
    static func showAlert(title: String, message: String, viewController: UIViewController, action: [UIAlertAction]? = nil, handler: ((UIAlertAction) -> Void)? = nil) {
        var actions = [UIAlertAction]()
        if let allAction = action {
            actions = allAction
        } else {
            actions.append(UIAlertAction(title: StringConstants.Message.Button.strOk, style: .default, handler: nil))
        }
        DialogVC.showPopup(title: title, message: message, parentVC: viewController, action: actions) { dialogAction in
            if let handler = handler {
                handler(dialogAction)
            }
        }
        
//        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: StringConstants.Message.Button.strOk, style: .default, handler: handler))
//        viewController.present(alert, animated: true, completion: nil)
    }

    static func showAlert(title: String, message: String, viewController: UIViewController, handler: ((UIAlertAction) -> Void)? = nil) {
        var actions = [UIAlertAction]()
//        if let action = action {
//            actions = action
//        } else {
//            actions.append(UIAlertAction(title: StringConstants.Message.Button.strOk, style: .default, handler: nil))
//        }
        actions.append(UIAlertAction(title: StringConstants.Message.Button.strOk, style: .default, handler: nil))
        DialogVC.showPopup(title: title, message: message, parentVC: viewController, action: actions) { dialogAction in
            if let handler = handler {
                handler(dialogAction)
            }
        }
    }

    //MARK:- Validate Password
    static func isStrongPassword(sPassword: String) -> Bool {
        var passRegex = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$&*]).{6,24}$"
        passRegex = "^(?=.*[A-Z,a-z])(?=.*[0-9])(?=.*[!@#$&*]).{6,24}$"
        let passTest = NSPredicate(format: "SELF MATCHES %@", passRegex)
        return passTest.evaluate(with: sPassword)
    }

    
    static func isEmailValid(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }

    static func isMobileNumberValid(_ mobileNumber: String) -> Bool {
        let mobileNumberRegEx = "\\d{10}"
        let mobileNumberPred = NSPredicate(format:"SELF MATCHES %@", mobileNumberRegEx)
        return mobileNumberPred.evaluate(with: mobileNumber)
    }

}

var vSpinner : UIView?

extension UIViewController { // https://brainwashinc.com/2017/07/21/loading-activity-indicator-ios-swift/

    func showSpinner(onView : UIView) {
        if vSpinner != nil {
            return
        }
        print("adding spinner")
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            print("removing spinner")
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }

    func showSpinner() {
        showSpinner(onView: view)
    }
    
}
extension UIImageView {

    func loadUrl(url: String, defaultImageType: DefaultImageType = .otherImage, complitionHandler: ( (UIImage?) -> Void)? = nil) {
        print("loading image from url str: \(url)")
//        self.sd_imageIndicator = SDWebImageActivityIndicator()
        self.sd_setImage(with: URL(string: url), completed: { image, error, cacheType, imageURL in
            if let complitionHandler = complitionHandler {
                print(error as Any)
                if error == nil {
                    complitionHandler(image)
                }
            }
            if error != nil {
                switch defaultImageType {
                case .profilePic:
                    self.image = UIImage(named: "profile_pic_default")
                case .otherImage:
                    self.image = UIImage(named: "img_default")
                }
            }
        })
    }
}
class Device {
  // Base width in point, use iPhone 6
    static let wBase: CGFloat = 414
    static let hBase: CGFloat = 896
    static var hRatio: CGFloat {
      return UIScreen.main.bounds.height / hBase
    }
    static var wRatio: CGFloat {
      return UIScreen.main.bounds.width / wBase
    }
}

extension CGFloat {
    var wAdjusted: CGFloat {
      return self * Device.wRatio
    }
    var hAdjusted: CGFloat {
      return self * Device.hRatio
    }
}
extension Double {
    var wAdjusted: CGFloat {
      return CGFloat(self) * Device.wRatio
    }
    var hAdjusted: CGFloat {
      return CGFloat(self) * Device.hRatio
    }
    
    func uptoTwoDecimal() -> String {
        return String(format: "%.2f", self)
    }
}
extension UIView
{
        @IBInspectable var cornerRadius: CGFloat {
            get { return layer.cornerRadius }
            set {
                  layer.cornerRadius = newValue

                  // If masksToBounds is true, subviews will be
                  // clipped to the rounded corners.
                  layer.masksToBounds = (newValue > 0)
            }
        }
    
    @IBInspectable var borderWidth: CGFloat {
            set {
                layer.borderWidth = newValue
            }
            get {
                return layer.borderWidth
            }
        }
@IBInspectable var borderColor: UIColor? {
            set {
                guard let uiColor = newValue else { return }
                layer.borderColor = uiColor.cgColor
            }
            get {
                guard let color = layer.borderColor else { return nil }
                return UIColor(cgColor: color)
            }
        }
}
extension Int {
    var wAdjusted: CGFloat {
      return CGFloat(self) * Device.wRatio
    }
    var hAdjusted: CGFloat {
      return CGFloat(self) * Device.hRatio
    }
    func uptoTwoDecimal() -> String {
        return String(format: "%.2f", self)
    }
}


final class HRationNSLayoutConstraint: NSLayoutConstraint {
    override var constant: CGFloat {
        set {
            super.constant = newValue
        }
        get {
            return super.constant.hAdjusted
        }
    }
}


final class WRationNSLayoutConstraint: NSLayoutConstraint {
    override var constant: CGFloat {
        set {
            super.constant = newValue
        }
        get {
            return super.constant.wAdjusted
        }
    }
}

enum UserType: String {
    case trainer, user
}

let USER_TYPE: UserType = UserType(rawValue: (Bundle.main.object(forInfoDictionaryKey: "USER_TYPE") as? String) ?? UserType.trainer.rawValue) ?? UserType.trainer

let BASE_URL: String = Bundle.main.object(forInfoDictionaryKey: "BASE_URL") as! String
let FACEBOOK_APP_ID: String = Bundle.main.object(forInfoDictionaryKey: "FacebookAppID") as! String
let URL_TYPES: Array = Bundle.main.object(forInfoDictionaryKey: "CFBundleURLTypes") as! Array<Any>

enum LoginType: String {
    case facebook, email, gmail, apple
}
enum DefaultImageType {
    case profilePic, otherImage
}
