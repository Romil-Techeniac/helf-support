//
//  AppManager.swift
//  Helf
//
//  Created by Hardik's Mac on 08/04/21.

import Foundation
import Alamofire
import SwiftyJSON
import NotificationView
import PushKit
import TwilioVoice
import TwilioConversationsClient
import SwiftyUserDefaults
let appManager  = AppManager.sharedInstance
let MainStoryboard = UIStoryboard(name: "Main", bundle: nil)
var voipRegistry = PKPushRegistry.init(queue: DispatchQueue.main)
var isUpdateAvailability : Bool = false
class AppManager: NSObject {
    
    var isTabChange : Bool = false
    var NoticationData = JSON()
    var GlobleAccsesToken = ""
    var DeviceToken = Data()
    
    var CallFrom = ""
    var activeCall: Call? = nil
    var TotalUnreadmsgCount : Int = 0
    var trainingPlaceName : String = ""
    static let sharedInstance = AppManager();
    
    var headersHTTP: HTTPHeaders {
        return [// "accept": "application/json",
            "os-name": "\(UIDevice.current.name)",
            "os-version": "\(UIDevice.current.systemVersion)",
            "platform": "iOS",
            "AppVersion": "\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] ?? "0.0")",
            "AppBuildVersion": "\(Bundle.main.infoDictionary?["CFBundleVersion"] ?? "0.0")",
            "Authorization": "Bearer \(Defaults.accessToken)"
//            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzcxY2NlZTJlNzdkYTBhNmI4NTczZGQ5NWRjMjQ4NWUwLTE2MzA4OTg1MzYiLCJpc3MiOiJTSzcxY2NlZTJlNzdkYTBhNmI4NTczZGQ5NWRjMjQ4NWUwIiwic3ViIjoiQUM3MmE0MzA3NTAyOWJlYWMxOTBiOTkxYzc2NzMxNzhkMiIsImV4cCI6MTYzMDkwMjEzNiwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiYWRtaW4iLCJjaGF0Ijp7InNlcnZpY2Vfc2lkIjoiSVMxZmNhMTBkNmY4ZDA0YTYxOTU5NzRkZDg5YTI0NmFjMiIsInB1c2hfY3JlZGVudGlhbF9zaWQiOiJDUmIyMTM5ODAzZjU1ZjRkN2JjYmNmNzI0YzFjNmEwMTVlIn19fQ.3f8xuMU0WxJwfc58GYHr7eCOrL7spCLg9Qu6SKwZEqc"
        ]
    }
    
    class func getToken( complition :@escaping ()->())
    {
        AppManager.sharedInstance.TotalUnreadmsgCount = 0
        var isdebug = ""
        #if DEBUG
        isdebug = "true"
        #else
        isdebug = "false"
        #endif

        var param = Parameters()
        param["is_token_expiry"] = "true"
//        param["is_debug"] = "true"
        
        NetworkClient.sharedInstance.request(StringConstants.serverURL, command: "/admin/chat_token", method: .get, parameters: param, headers: appManager.headersHTTP) { (responce, Message) in
            print(responce,Message)
            if let responceDict = responce as? [String : Any]{
                Defaults.accessToken = responceDict["token"] as! String
                QuickstartConversationsManager.shared.login(token: "\(Defaults.accessToken)"){ result in
                    print("--**>",result)
                    let Conversations = QuickstartConversationsManager.shared.client?.myConversations() ?? []
                    print(QuickstartConversationsManager.shared.client as Any)
                    for Conversation in Conversations{
                        Conversation.getUnreadMessagesCount(completion: { (result, count) in
                            print(result)
                            print(count as Any)
                            if count == nil{
                                Conversation.getMessagesCount { (result, count) in
                                    print(Conversation.sid as Any,"unread", count)
                                    AppManager.sharedInstance.TotalUnreadmsgCount = AppManager.sharedInstance.TotalUnreadmsgCount + Int(count)
                                }
                            }else{
                                AppManager.sharedInstance.TotalUnreadmsgCount = AppManager.sharedInstance.TotalUnreadmsgCount + Int(truncating: count!)
                            }
                            
                        })
                        
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        print("TOTAL",AppManager.sharedInstance.TotalUnreadmsgCount)
                        complition()
                    }
                }
            }
        } failure: { (error, code) in
            let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstants.Message.Button.strOk, style: .default, handler: nil))
            UIApplication.shared.topMostViewController()?.present(alert, animated: true, completion: nil)
        }
        
        //        TwilioConversationsClient().register(withNotificationToken: Data(Defaults[\.fcmToken].utf8)) { (result) in
        //            print(result.isSuccessful)
        //            print(result.error)
        //        }
    }
    
    class func GetcurrentTime() -> Date {
        let today1 = Date().Tostring(format: "MM-dd-yyyy HH:mm:ss")
        return today1.toDate(withFormat: "MM-dd-yyyy HH:mm:ss")!
    }
        class func OnScreennotificaiton(Test:String,Subtitle:String)
        {
            let notificationView = NotificationView()
            notificationView.title = "Message"
            notificationView.body = Subtitle
            notificationView.theme = .default
            notificationView.isHeader = true
            notificationView.btnDetail = {
                AppManager.NotificationPush(Obj: appManager.NoticationData)
//                appManager.NoticationData = JSON()
            }
//            if USER_TYPE == .user{
//                if UserHomeTabVC() == UIApplication.shared.topMostViewController(){
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadHomeData"), object: nil)
//                }
//            }
            notificationView.show()
        }
        class func OnScreennotificaitonSession(Title:String,body:String)
        {
            let notificationView = NotificationView()
            notificationView.title = Title
            notificationView.body = body
            notificationView.theme = .default
            notificationView.isHeader = true
            notificationView.btnDetail = {
                AppManager.NotificationPush(Obj: appManager.NoticationData)
//                appManager.NoticationData = JSON()
            }
            notificationView.show()
        }
//        class func clearRegistrationModel() {
//            userRegistrationRequestModel = UserRegistrationRequestModel(loginType: .email)
//        }
    class func NotificationPush(Obj:JSON) {
        if Obj["twi_message_id"].stringValue != ""{

            let client = searchClientDataList.filter({$0.twilioChannelSid == (Obj["conversation_sid"].stringValue)}).first
            let vc = ChatVC.init(nibName: ChatVC.IDENTIFIER, bundle: nil)
            sid = client?.twilioChannelSid ?? ""
            identity = "\(client?.id ?? 0)_\(client?.appNo ?? "")"
            vc.strName = "\(client?.firstName ?? "") \(client?.lastName ?? "")"
            vc.strImg = client?.profilePicture ?? ""
            vc.trainerOrClientid = client?.id ?? 0
            appManager.NoticationData = JSON()
//            let vc = ClientListVC.init(nibName: ClientListVC.IDENTIFIER, bundle: nil)
//            vc.strTitle = "Your Client"
//            vc.isFromNotification = true
//            vc.conversation_sid = Obj["conversation_sid"].stringValue
//            vc.modalPresentationStyle = .fullScreen
            UIApplication.shared.topMostViewController()?.navigationController?.pushViewController(vc, animated: true)
        }else{
            Utils.showAlert(title: Obj["aps"]["alert"]["title"].stringValue, message: Obj["gcm.notification.text"].stringValue, viewController: UIApplication.shared.topMostViewController()!)
        }
    }
}
extension Date {
    func Tostring(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
