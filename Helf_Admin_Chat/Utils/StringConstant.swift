//
//  Constant.swift
//  GenericNetworkLayeriOS
//
//  Created by Yusuf Demirci on 13.04.2020.
//  Copyright © 2020 Yusuf Demirci. All rights reserved.
//

import Foundation

class Constant {}

struct StringConstants {
//    static let serverURL                            = "http://3.136.198.47/api" //Test Server
    static let serverURL                            = "https://helf.dev/api" //Live Server

    static let strHelf = "Helf"
    static let strHelfUser = "Helf User"
    static let strHelfTrainer = "Helf Trainer"
    static let strTrainer = "Trainer"
    static let strClient = "Client"
    struct Message {
        struct Button {
            static let strYes = "Yes"
            static let strNo = "No"
            static let strOk = "Ok"
            static let strCancel = "Cancel"
            static let strDone = "Done"
            static let strUpdateProfile = "Update Profile"
            static let strUpdate = "Update"
             static let strBook = "Book"
             static let strView = "View"
            

        }
        struct RatingPopup {
            static let strExcellent = "Excellent"
            static let strVeryGood = "Very Good"
            static let strGood = "Good"
            static let strNotGood = "Not Good"
            static let strDisappointed = "Disappointed"
        }
        
        struct PackageStatus {
            static let strPending = "pending"
            static let strCancel = "cancel"
            static let strUsed = "used"
            static let strSuccess = "success"
            static let strExpired = "expired"
            static let strPartialycancel = "partialy cancel"
            static let strcompleted = "completed"
        }
        struct SessionStatus {
            static let strPending = "pending"
            static let strCancel = "cancel"
            static let strInProgress = "in progress"
            static let strcompleted = "completed"
            static let strOnTheWay = "on the way"
            static let strReviewed = "reviewed"
            static let strMissed = "partialy cancel"
        }

        struct AlertTitle {
            static let strAlert = "Alert"
            static let strInvalid = "Invalid"
            static let strError = "Error"
            static let strDelete = "Delete"
            static let strLogout = "Logout"
            static let strMessage = "Message"
            static let strLogIn = "Log In"
            static let strUpdate = "Update"

            static let strUpcomingSessions = "Upcoming Sessions"
            static let strCancelSession = "Cancel Session"
            static let strCancelledSessions = "Cancelled Sessions"
            static let strSessionCanceled = "Session Canceled"
            static let strSessionExpired = "Session Expired"
            static let strMissedSessions = "Missed Sessions"
            static let strCompletedSessions = "Completed Sessions"
            
            static let strMyPackages = "My Packages"
            static let strCancelPackage = "Cancel Package"
            static let strCancelledPackages = "Cancelled Packages"
            static let strPackageCanceled = "Package Canceled"
            static let strPackageExpired = "Package Expired"
            static let strCompletedPackages = "Completed Packages"
            static let strExpiredPackages = "Expired Packages"
            
            static let strCommingSoon = "Comming Soon"
            static let strTermsAndConditions = "Terms & Conditions"
            static let strPayment = "Payment"
            static let strBookingSuccessful = "Booking Successful"
            static let strJoinSession = "Join Session"
            static let strFailed = "Failed"
            static let strSuccessful = "Successful"
            static let strDayOff = "Day Off"
            static let strWarning = "Warning"
            static let strUploadCertificate = "Upload Certificate"
            static let strUploadUploadGallary = "Upload Gallary"
            static let strOnTheWay = "On The Way"
            static let strNoInternet = "No Internet"
            static let strAppleSignIn = "Apple Sign In"
            static let strSettings = "Settings"
            static let strDeleteAccount = "Delete Account"
            static let strOnMyWay = "On My Way"
//            static let strOnMyWay = "On My Way"


        }
        
        static let strDelete  = "Are you sure you want to delete?"
        static let strLogout  = "Are you sure you want to logout?"
        static let strFailDeleteCertificate  = "Failed to delete Certificate"
        static let strCancelThisSession = "Are you sure, You want to cancel this session?"
        static let strNoGalleryImagesFound = "No Gallery Images found. Please upload some images"
        static let strFailedToDeleteGalleryImage = "Failed to delete Gallery Image"
        static let strCertificateUploaded = "Certificate successfully uploaded"
        static let strGalleryImageUploaded = "Gallery Image successfully uploaded"
        static let strGalleryVideoUploaded = "Gallery Video successfully uploaded"
        static let strFilesizeLessThen50MB = "size is too big. please select lessthen 50 MB"
        static let strAfterCompressLessthen100MB = "after compress also size is too big. please select lessthen 100 MB"
        static let strAtleastOneDay = "Please enable atleast one day"
        static let strValidHeadline = "Headline should be between 2 and 50 characters"
        static let strValidAboutMe = "About Me should be between 10 and 10000 characters"
        static let strVaccinationCertificate = "please Upload Vaccination Certificate"
        static let strSelectProfie = "Please select a profile picture"
        static let strSelectGender = "Please select gender"
        static let strOtpSend = "An otp is sent to your mobile number"
        static let strValidMobileNumber  = "Please enter valid Mobile Number. It should be 10 digit numeric input."
        static let strValidEmail  = "Please enter valid Email"
        static let strEnterPassword  = "Please enter password"
        static let strReset = "You password was successfully reset"
        static let strValidPassword  = "Password must be at least 6 characters, and contain at least one number and one special character. Passwords are case sensitive."
        static let strConfirmPassword  = "You entered a different password in Confirm Password Field"
        static let strTrainingType  = "Please select traing type"
        static let strValidRate  = "The Rate amount should be a valid numeric and greater than 0"
        static let strSelectExperiance  = "Please select year of experience"
        static let strSelectSkill  = "Please select atleast one Skill"
        static let strValidFirstName  = "First Name should be between 2 and 20 characters"
        static let strValidLastName  = "Last Name should be between 2 and 20 characters"
        static let strNoCertificateFound = "No Certificates found. Please upload some certificates"
        static let strEndDateMustBeGrater = "The end date must be a date after or equal to start date."
        static let strStartDateRequired = "The start date field is required."
        static let strEndDateRequired = "The end date field is required."
        static let strTnCTapped = "Terms and Conditions link tapped"
        static let strZoomNotAvailable = "Zoom Link is not Available"
        static let strOntheWay = "Your client is notified that you are on the way"
        static let strZoomNotOpen = "Zoom Link Can't open"
        static let strCheckInternet = "Please check your internet connection and try again"
        static let strChooesTimeSlot = "Please chooes any time slot"
        static let strAcceptTnC = "Please accept terms and conditions"
        static let strSelectTimeSlot = "Please select at least one time slot"
        static let strSelectTrainingType = "Please select at least one training type"
        static let strSelectTrainingPlace = "Please select Training Place"
        static let strPaymentFail = "Payment Fail"
        static let strPaymentCancel = "Payment Cancel"
        static let strUpdateProfile = "Successfully Updated Profile"
        static let strUnableGetEmail = "Unable to get your Email ID. Please change your privacy settings & try again."
        static let strTryAgain = "Please try again later"
        static let strCreateNewPassword = "Lets create a new password"
        static let strSetupProfile = "Lets setup you profile"
        static let strLetMakeMostOfThisApp = "Let make the most of this app"
        static let strSignupGreter13Alert = "Sign Up with Apple works only with OS version > 13. Please ugrade your phone's OS"
        static let strAppleSigninError = "Something went wrong with your Apple Sign In!"
        static let strTwilioConversationError = "something went wrong with this conversation!"
        static let strTwilioMessageError = "something went wrong with load. messages!"
        static let strRateYourExperiance = "please rate your experience"
        static let strDeleteAccount = "loos your all data. \n Are you sure to Delete your Account?"
        static let strPackageDataNotFatch = "package Data not Fatch"
        static let strCancelPackage = "Are you sure, You want to cancel this package? If you cancel this package, you will get refund amount of booked or pending sessions as Credit Balance."
        static let strUnableToGetAppleId = "We are not able to get e-mail from this Apple ID. Please make sure you have enabled sharing e-mail Id or Go to Settings>Apple ID>Password & Security>Apps using Apple ID and stop Helf User from Apple Id Logins"
        static let strCancelThisSessionAndRefund = "Are you sure, You want to cancel this session? You will get session back but if you cancel after the 12 hour cancellation grace period, you will lose your session."

        static let strAllCaughtUp = "All Caught Up!!"
        static let strNoSessionFound = "No Sessions Found"
        static let strNoPackageFound = "No Package Found"
        static let strOOOPS = "OOOPS!"
        static let strYouCanCheckNextDay = "You can check next day\nOr"
        static let strWhereShouldYourTrainerMeetYou = "Where should your trainer meet you?"
        static let strPleaseAllowAccessToYourLocation = "For more accurate services, please allow access to your location"
        static let strONTHEWAY = "ON THE WAY"
        static let strChooseYourHomeLocation = "Choose your home location"
        static let strHireUpTo15Mile = "Get hired up to 15 miles from your \"home location.\""
        static let strINPROGRESS = "IN PROGRESS"
        static let strAtHome = "At Home"
        static let strOnline = "Online"
        static let strAbout = "About"
        static let strAboutMe = "About Me"
        static let strReciveOtp = "You will receive OTP."
        static let strSendNewCode = "Send a new code"
        static let strONGOING = "ONGOING"
        static let strCurrentSession = "Current Session"
        static let strAccountHasBeenCreated = "Congrats!\nYour account has been created"
        static let strSubmitReviewAboutTrainer = "Please submit your feedback about trainer."
        static let strSubmitReviewAboutClient = "Please submit your feedback about client."
        static let strNoRatingReviewFound = "No Rating & Review Found"
        static let strNoConnectionFoundUser = "No Connections found. You will be able to chat with trainer once you purchase any package."
        static let strNoConnectionFoundTrainer = "No Connections found. You will be able to chat once any user purchases your packages."
        static let strSelectDiffranceTimeSlot = "Please keep some difference of time between timeslots or merge timeslots"
        static let strUpdateTrainingAvailability = "You can now enter multiple time slots to each day. Kindly update your old daily availability and add multiple if required."
        static let strErrorTrainingAvailability = "There was some error with this trainer availability"
//        static let strCurrentSession = "Current Session"

    }
}
