//
//  AppDelegate.swift
//  Helf_Admin_Chat
//
//  Created by Techeniac Services on 02/09/21.
//

import UIKit
import CoreData
import UserNotifications
import SwiftyJSON
@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "335958359353"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        initialConfig()
        notificationSetup(application)
//        let remoteNotif = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: Any]

//        if remoteNotif != nil {
//            let Obj = JSON(remoteNotif as Any)
////            AppManager.getToken {
////                AppManager.na(Obj: Obj)
//            appManager.NoticationData = Obj
////            }
//            let vc = ClientListVC.init(nibName: ClientListVC.IDENTIFIER, bundle: nil)
//            UIApplication.shared.topMostViewController()?.navigationController?.pushViewController(vc, animated: true)
//
////            AppManager.NotificationPush(Obj: Obj)
//        }

        return true
    }
    
    // MARK: - Core Data stack
    
    func initialConfig() {
        //Initiate window
        window = UIWindow.init(frame: UIScreen.main.bounds)
        
        //Initiate View Controller
        let vc = SplashVC.init(nibName: SplashVC.IDENTIFIER, bundle: nil)
        let root = UINavigationController.init(rootViewController: vc)
        
        root.navigationBar.isHidden = true
        window?.rootViewController = root
        window?.makeKeyAndVisible()
        
    }
    
    func notificationSetup(_ application: UIApplication){
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
//        application.registerForRemoteNotifications()
        
    }
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Helf_Admin_Chat")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension AppDelegate
{
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        appManager.DeviceToken = deviceToken
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        print("Received device token")
        
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        // Print the error to console (you should alert the user that registration failed)
        
        print("APNs registration failed: \(error)")
        
    }
    
}
@available(iOS 10, *)
extension AppDelegate {
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Push notification received:")
        print(userInfo)
        let Obj = JSON(userInfo)
        
        // Print message ID.
//        if Obj["google.c.sender.id"].stringValue == "335958359353" {
//            appManager.NoticationData = Obj
//        }
//        AppManager.NotificationPush(Obj: Obj)
//        AppManager.getToken {
//            AppManager.NotificationPush(Obj: Obj)
//        }

    }
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        let Obj = JSON(userInfo)
            let Message = Obj["aps"]["alert"]["body"].stringValue
            appManager.NoticationData = Obj
            AppManager.OnScreennotificaiton(Test: "from : \(Obj["author"].stringValue)", Subtitle:Message)
   
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
         let Obj = JSON(userInfo)
        print(Obj)
        // Print message ID.
        if Obj["twi_message_id"].stringValue != ""
        {
            AppManager.NotificationPush(Obj: Obj)
            appManager.NoticationData = Obj
        }
        print(userInfo)
        completionHandler()
    }
}
