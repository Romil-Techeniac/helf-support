//
//  ChatUserListResponceModel.swift
//  Helf
//
//  Created by Techeniac Services on 08/04/21.
//

import Foundation
import TwilioConversationsClient
import ObjectMapper

struct ChatUserListResponceModel: Mappable {
    var appNo : String?
    var email : String?
    var firstName : String?
    var id : Int?
    var lastName : String?
    var profilePicture : String?
    var twilioChannelSid : String?
    var conversation : TCHConversation?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
         appNo <- map["app_no"]
         email <- map["email"]
         firstName <- map["first_name"]
         id <- map["id"]
         lastName <- map["last_name"]
         profilePicture <- map["profile_picture"]
         twilioChannelSid <- map["twilio_channel_sid"]
    }
    
    
}
//struct ChatUserListResponceModel : Codable {
//
//        let appNo : String?
//        let email : String?
//        let firstName : String?
//        let id : Int?
//        let lastName : String?
//        let profilePicture : String?
//        let twilioChannelSid : String?
//        var conversation : TCHConversation?
//
//        enum CodingKeys: String, CodingKey {
//                case appNo = "app_no"
//                case email = "email"
//                case firstName = "first_name"
//                case id = "id"
//                case lastName = "last_name"
//                case profilePicture = "profile_picture"
//                case twilioChannelSid = "twilio_channel_sid"
//        }
//}
