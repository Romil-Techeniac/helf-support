//
//  DialogVC.swift
//  Helf_Admin_Chat
//
//  Created by Techeniac Services on 02/09/21.
//

import UIKit

class DialogVC: UIViewController {
    
    static let IDENTIFIER = "DialogVC"
    
    @IBOutlet weak var vNoView: UIView!
    @IBOutlet weak var vYesView: UIView!
    
    var titleStr = ""
    var messageStr = ""
    var handler: ((UIAlertAction) -> Void)?
    @IBOutlet weak var vDialogRoot: UIView!
    
    @IBOutlet weak var labelYes: UILabel!
    @IBOutlet weak var labelNo: UILabel!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    
    var action = [UIAlertAction]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.50)
        
        vDialogRoot.layer.cornerRadius = 10
        vDialogRoot.clipsToBounds = true
        
        vNoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(noButtonTapped)))
        vNoView.isUserInteractionEnabled = true
        
        vYesView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(yesButtonTapped)))
        vYesView.isUserInteractionEnabled = true
        
        vNoView.isHidden = true
        labelYes.text = "OK"
        
        labelTitle.text = titleStr
        labelMessage.text = messageStr
        
        if action.count > 0 {
            let currentAction = action[0]
            labelNo.text = currentAction.title
            if currentAction.style == .destructive {
                labelNo.textColor = .red
            } else {
                labelNo.textColor = UIColor.init(hex: "#0747ADFF")
            }
            vNoView.isHidden = false
        } else {
            vNoView.isHidden = true
        }
        if action.count > 1 {
            let currentAction = action[1]
            labelYes.text = currentAction.title
            if currentAction.style == .destructive {
                labelYes.textColor = UIColor.init(red: 1, green: 0, blue: 0, alpha: 1)
            } else {
                labelYes.textColor = UIColor.init(hex: "#0747ADFF")
            }
            vYesView.isHidden = false
        } else {
            vYesView.isHidden = true
        }
    }
    
    @objc func noButtonTapped() {
        dismiss(animated: true, completion: nil)
        if let handler = handler {
            if action.count > 0 {
                handler(action[0])
            }
        }
    }

    @objc func yesButtonTapped() {
        dismiss(animated: true, completion: nil)
        if let handler = handler {
            if action.count > 1 {
                handler(action[1])
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- functions for the viewController
    static func showPopup(title: String, message: String, parentVC: UIViewController, action: [UIAlertAction]? = nil, handler: ((UIAlertAction) -> Void)? = nil) {
        //creating a reference for the dialogView controller
        let popupViewController = DialogVC.init(nibName: DialogVC.IDENTIFIER, bundle: nil) 
            popupViewController.modalPresentationStyle = .custom
            popupViewController.modalTransitionStyle = .crossDissolve
            popupViewController.titleStr = title
            popupViewController.messageStr = message
            popupViewController.handler = handler
            popupViewController.action = action ?? popupViewController.action
            
            //setting the delegate of the dialog box to the parent viewController
//            popupViewController.delegate = parentVC as? PopUpProtocol

            //presenting the pop up viewController from the parent viewController
            parentVC.present(popupViewController, animated: true)
    }

}
