//
//  ClientListVC.swift
//  Helf_Admin_Chat
//
//  Created by Techeniac Services on 02/09/21.
//

import UIKit
import ObjectMapper
import SwiftyUserDefaults
import TwilioConversationsClient
import NVActivityIndicatorView
import SwiftyJSON

var searchClientDataList = [ChatUserListResponceModel]()
class ClientListVC: UIViewController {
    static let IDENTIFIER = "ClientListVC"
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnInfo : UIButton!
    @IBOutlet weak var btnBack : UIButton!
    @IBOutlet weak var txtsearch : UITextField!
    @IBOutlet weak var viewMessage : UIView!
    @IBOutlet weak var btnsearch : UIButton!
    @IBOutlet weak var tblUserList : UITableView!
    @IBOutlet weak var vwNodata: UIView!
    @IBOutlet weak var lblErrorMSG: UILabel!
    
    var clientDataList = [ChatUserListResponceModel]()
    var Conversations :[TCHConversation] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtsearch.delegate = self
        txtsearch.layer.cornerRadius = txtsearch.frame.height / 2
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height:  self.txtsearch.frame.height))
        txtsearch.leftView = paddingView
        txtsearch.leftViewMode = .always
        tblUserList.register(UINib(nibName: ClientCell.IDENTIFIER, bundle: .main), forCellReuseIdentifier: ClientCell.IDENTIFIER)
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        callApiForTwillioLogin{
            self.Conversations = QuickstartConversationsManager.shared.client?.myConversations() ?? []
            self.callApi()
        }
    }
    func callApi() {
        NetworkClient.sharedInstance.request(StringConstants.serverURL, command: "/admin/get_list", method: .get, headers: appManager.headersHTTP) { [self] (response, message) in
            //            print(response)
            if let dictResponse = response as? [[String:Any]]{
                clientDataList = Mapper<ChatUserListResponceModel>().mapArray(JSONArray: dictResponse)
            }
            for (index,client)in self.clientDataList.enumerated(){
                let singleConversation = self.getConversation(sid: client.twilioChannelSid!)
                var obj = client
                obj.conversation = singleConversation.first
                self.clientDataList[index] = obj
            }
            self.clientDataList = self.clientDataList.sorted{ $0.conversation?.lastMessageDate ?? Date(timeIntervalSince1970: 0) > $1.conversation?.lastMessageDate ?? Date(timeIntervalSince1970: 0)
            }
            searchClientDataList = clientDataList
            if appManager.NoticationData["twi_message_id"].stringValue != ""
            {
                print("from notification")
                let client = searchClientDataList.filter({$0.twilioChannelSid == (appManager.NoticationData["conversation_sid"].stringValue)}).first!
                let vc = ChatVC.init(nibName: ChatVC.IDENTIFIER, bundle: nil)
                sid = client.twilioChannelSid ?? ""
                identity = "\(client.id ?? 0)_\(client.appNo ?? "")"
                vc.strName = "\(client.firstName ?? "") \(client.lastName ?? "")"
                vc.strImg = client.profilePicture ?? ""
                vc.trainerOrClientid = client.id ?? 0
                appManager.NoticationData = JSON()
                navigationController?.pushViewController(vc, animated: true)
            }
            
            tblUserList.reloadData()
        } failure: { (error, code) in
            print(error, code)
            let alert = UIAlertController(title: StringConstants.Message.AlertTitle.strAlert, message: error, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstants.Message.Button.strOk, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    func getConversation(sid :String) -> [TCHConversation]{
        for conversation in Conversations{
            if conversation.sid == sid{
                return [conversation]
            }
        }
        return []
    }
    
    func callApiForTwillioLogin(complition :@escaping ()->()) {
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0)
        
        QuickstartConversationsManager.shared.login(token: "\(Defaults.accessToken)"){ result in
            print("--**>",result)
            let Conversations = QuickstartConversationsManager.shared.client?.myConversations() ?? []
            print(QuickstartConversationsManager.shared.client as Any)
            for Conversation in Conversations{
                Conversation.getUnreadMessagesCount(completion: { (result, count) in
                    print(result)
                    print(count as Any)
                    if count == nil{
                        Conversation.getMessagesCount { (result, count) in
                            print(Conversation.sid as Any,"unread", count)
                            AppManager.sharedInstance.TotalUnreadmsgCount = AppManager.sharedInstance.TotalUnreadmsgCount + Int(count)
                        }
                    }else{
                        AppManager.sharedInstance.TotalUnreadmsgCount = AppManager.sharedInstance.TotalUnreadmsgCount + Int(truncating: count!)
                    }
                })
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                print("TOTAL",AppManager.sharedInstance.TotalUnreadmsgCount)
                complition()
            }
        }
    }
    @IBAction func btnLogout_Click(){
        Utils.showAlert(title: StringConstants.Message.AlertTitle.strLogout, message: StringConstants.Message.strLogout, viewController: self, action: [UIAlertAction(title: StringConstants.Message.Button.strNo, style: .default, handler: nil), UIAlertAction(title: StringConstants.Message.Button.strYes, style: .destructive, handler: nil)]) { (action) in
            if action.title == StringConstants.Message.Button.strYes{
                Defaults.isLoggedIn = false
                QuickstartConversationsManager.shared.unRegisterToken(fcmToken: Defaults.accessToken)
                QuickstartConversationsManager.shared.client = nil
                UIApplication.shared.unregisterForRemoteNotifications()

                self.navigationController?.popToRootViewController(animated: true)
            }else if action.title == StringConstants.Message.Button.strNo{
                print("cancel")
            }
        }
    }
}

extension ClientListVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.vwNodata.isHidden = searchClientDataList.count > 0
        return searchClientDataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ClientCell.IDENTIFIER, for: indexPath) as! ClientCell
        cell.setData(model: searchClientDataList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        view.endEditing(true)
        let vc = ChatVC.init(nibName: ChatVC.IDENTIFIER, bundle: nil)
        sid = searchClientDataList[indexPath.row].twilioChannelSid ?? ""
        identity = "\(searchClientDataList[indexPath.row].id ?? 0)_\(searchClientDataList[indexPath.row].appNo ?? "")"
        vc.strName = "\(searchClientDataList[indexPath.row].firstName ?? "") \(searchClientDataList[indexPath.row].lastName ?? "")"
        vc.strImg = searchClientDataList[indexPath.row].profilePicture ?? ""
        vc.trainerOrClientid = searchClientDataList[indexPath.row].id ?? 0
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension ClientListVC: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let searchText  = textField.text! + string
        if searchText.count > 1 {
            print(searchText)
            searchClientDataList = self.clientDataList.filter({
                (($0.firstName!).localizedCaseInsensitiveContains(searchText)) || (($0.lastName!).localizedCaseInsensitiveContains(searchText))
                
            })
            tblUserList.reloadData()
        }
        else{
            searchClientDataList = clientDataList
            tblUserList.reloadData()
        }
        return true
    }
}
