//
//  ClientCell.swift
//  Helf
//
//  Created by Techeniac Services on 08/04/21.
//

import UIKit

class ClientCell: UITableViewCell {
    static let IDENTIFIER = "ClientCell"
    
    //MARK: Initialize IBOutlet
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var imgStatus : UIImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var imgStar : UIImageView!
    @IBOutlet weak var lblRate : UILabel!
    @IBOutlet weak var lblMessage : UILabel!
    @IBOutlet weak var lblMsgCount : UILabel!
    @IBOutlet weak var viewMain : UIView!
    @IBOutlet weak var viewSeprator: UIView!
    @IBOutlet weak var viewWidthConstraint: NSLayoutConstraint!
    
    
    //MARK: Initialize Variable
    var btnDeleteRecord : (()->())?
    
    //MARK: Initialize methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgStatus.isHidden = true
        imgProfile.layer.cornerRadius = imgProfile.layer.frame.height / 2
        imgProfile.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setData(model : ChatUserListResponceModel){
        
        var clientCount : Int = 0
        var clientLastMessage : String = ""
        lblMsgCount.cornerRadius = lblMsgCount.frame.height / 2
        imgStar.isHidden = true
        lblRate.isHidden = true
        imgProfile.loadUrl(url: model.profilePicture ?? "")
        lblName.text = "\(model.firstName ?? "") \(model.lastName ?? "")"
        viewSeprator.isHidden = true
        
        model.conversation?.getUnreadMessagesCount(completion: { (_, count) in
            print(count as Any)
            if count == nil{
                model.conversation?.getMessagesCount(completion: { (_, count) in
                    clientCount = Int(count)
                })
            }else{
                clientCount = count as! Int
            }
            if clientCount == 0{
                self.lblMsgCount.isHidden = true
            }else if clientCount >= 99{
                self.lblMsgCount.isHidden = false
                self.lblMsgCount.text = "99+"
            }else{
                self.lblMsgCount.isHidden = false
                self.lblMsgCount.text = "\(clientCount)"
            }
            print(clientCount)
        })
        model.conversation?.getLastMessages(withCount: 1, completion: { (_, message) in
            if let message = message?.first, message.hasMedia(){
                if (message.mediaType?.contains("image") ?? false){
                    clientLastMessage = "Image"
                }else if (message.mediaType?.contains("video") ?? false){
                    clientLastMessage = "Video"
                }
            }else{
                clientLastMessage = message?.first?.body ?? ""
            }
            print(clientLastMessage)
            if clientLastMessage == ""{
                self.lblMessage.isHidden = true
            }else{
                self.lblMessage.isHidden = false
            }
            self.lblMessage.text = clientLastMessage
            
        })
        
    }
//MARK: Initialize IBActions
    
}
