//
//  ChatVC.swift
//  Helf_Admin_Chat
//
//  Created by Techeniac Services on 02/09/21.
//

import UIKit
import TwilioConversationsClient
import SDWebImage

var identity = "admin"
var sid = ""
class MessageCell: UITableViewCell {
    @IBOutlet weak var lblMsg : PaddingLabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var btnimage : UIButton!
    @IBOutlet weak var imgimage : UIImageView!
    @IBOutlet weak var viewPlayimage : UIView!
    @IBOutlet weak var imgSenderTag : UIImageView!
    @IBOutlet weak var viewMedia : UIView!
    @IBOutlet weak var btnSelect: BtnTwilo!
}
class BtnTwilo: UIButton {
    var ImgUrl = ""
    var type = ""
    var Indexpath = IndexPath()
}

class ChatVC: UIViewController {
    static let IDENTIFIER = "ChatVC"
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewCall : UIView!
    @IBOutlet weak var ivAttachement: UIImageView!
    @IBOutlet weak var btnSend: UIButton!
    
    var strName = ""
    var strImg = ""
    var trainerOrClientid = Int()
    var imagePicker: ImagePicker!
    var strTitle = String()
    var isFromNotification = Bool()
    var conversation_sid = String()
    private(set) var messages: [TCHMessage] = []
    private var conversation: TCHConversation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Listen for keyboard events and animate text field as necessary
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow),
                                               name: UIResponder.keyboardDidShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
        
        // Do any additional setup after loading the view.
        self.imagePicker = ImagePicker(presentationController: self, delegate: self, mediaTypes: ["public.image", "public.movie"])
        
        ivAttachement.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(btnAttechment_Click(sender:))))
        ivAttachement.isUserInteractionEnabled = true
        
        tableView.register(UINib(nibName: CallInfoCell.IDENTIFIER, bundle: nil), forCellReuseIdentifier: CallInfoCell.IDENTIFIER)
        tableView.register(UINib(nibName: MessageSenderCell.IDENTIFIER, bundle: nil), forCellReuseIdentifier: MessageSenderCell.IDENTIFIER)
        tableView.register(UINib(nibName: MessageReceiverCell.IDENTIFIER, bundle: nil), forCellReuseIdentifier: MessageReceiverCell.IDENTIFIER)
        
        QuickstartConversationsManager.shared.delegate = self
        lblName.text = strName
        imgProfile.loadUrl(url: strImg)
        loadChat()
    }
    func loadChat(){
        DispatchQueue.main.async {
            QuickstartConversationsManager.shared.checkConversationCreation(conversation_sid: sid) { (result, conversation) in
                if let conversation = conversation {
                    self.conversation = conversation
                    self.loadPreviousMessages(conversation)
                } else {
                    Utils.showAlert(title: StringConstants.Message.AlertTitle.strError, message: StringConstants.Message.strTwilioConversationError, viewController: self, action: [UIAlertAction(title: StringConstants.Message.Button.strOk, style: .destructive, handler: {_ in
                        self.navigationController?.popToRootViewController(animated: true)
                    })]) { action in
                        if action.title == StringConstants.Message.Button.strOk {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    print("not conversation")
                }
            }
        }
    }
    func loadPreviousMessages(_ conversation: TCHConversation) {
        print(conversation)
        conversation.getLastMessages(withCount: 200) { (result, messages) in
            print("MSG",messages as Any)
            let lastReadCount = (messages?.last?.index)
            conversation.setLastReadMessageIndex(lastReadCount ?? 0) { (result, number) in
                print("setLastReadMessageIndex",result)
            }
            
            if let messages = messages {
                self.messages = messages
                DispatchQueue.main.async {
                    self.reloadConversationMessages()
                }
            }else{
                Utils.showAlert(title: StringConstants.Message.AlertTitle.strError, message: StringConstants.Message.strTwilioMessageError, viewController: self, action: [UIAlertAction(title: StringConstants.Message.Button.strOk, style: .destructive, handler: {_ in
                    self.navigationController?.popToRootViewController(animated: true)
                })]) { action in
                    if action.title == StringConstants.Message.Button.strOk {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                print("MSG NOT LOAD")
            }
        }
    }
    func reloadConversationMessages() {
        print("reloadMessages...")
        self.tableView.reloadData()
        scrollToBottom()
        removeSpinner()
    }
    func scrollToBottom(){
        DispatchQueue.main.async {
            if self.messages.count != 0{
                let indexPath = IndexPath(row: self.messages.count-1, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    // MARK: Keyboard Dodging Logic
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey]
                                as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.bottomConstraint.constant = keyboardRect.height + 10
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        scrollToBottomMessage()
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConstraint.constant = 20
            self.view.layoutIfNeeded()
        })
    }
    @objc func btnAttechment_Click(sender : UIImageView) {
        self.imagePicker.present(from: sender)
    }
    
    @IBAction func btnBack_Click(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func BtnSelectCellTapped(Btn:BtnTwilo)
    {
        view.endEditing(true)
        print(Btn.ImgUrl)
        print(Btn.type)
        let str = Btn.ImgUrl+","+Btn.type
        print(Btn.Indexpath)
        if !Btn.ImgUrl.isEmpty{
            CSFileBrowser.openAttachmentSlider(navigation: self.navigationController!, attachment: [str])
        }
    }
    @IBAction func btnSend_Click(_ sender: Any) {
        if (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count)! > 0{
            QuickstartConversationsManager.shared.sendMessage(self.conversation!, textField.text!.trimmingCharacters(in: .whitespacesAndNewlines), completion: { (result, _) in
                if result.isSuccessful {
                    self.textField.text = ""
                    self.textField.resignFirstResponder()
                } else {
                    self.displayErrorMessage("Unable to send message")
                }
            })
        }else{
            print("type msg with minimun 1 character")
            self.textField.text = ""
            self.textField.resignFirstResponder()
        }
        
    }
    internal func displayErrorMessage(_ errorMessage: String) {
        let alertController = UIAlertController(title: StringConstants.Message.AlertTitle.strError,
                                                message: errorMessage,
                                                preferredStyle: .alert)
        let okAction = UIAlertAction(title: StringConstants.Message.Button.strOk, style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    private func scrollToBottomMessage() {
        if self.messages.count == 0 {
            return
        }
        let bottomMessageIndex = IndexPath(row: self.messages.count - 1,
                                           section: 0)
        tableView.scrollToRow(at: bottomMessageIndex, at: .bottom, animated: true)
    }
}
extension ChatVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // Return number of rows in the table
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
        
    }
    
    // Create table view rows
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
    -> UITableViewCell {
        
        let message = self.messages[indexPath.row]
        //        let message = TCHMessage()
        //        // place call log text here
        if message.body?.localizedCaseInsensitiveContains("You had a call") ?? false || message.body?.localizedCaseInsensitiveContains("Rejected call")  ?? false || message.body?.localizedCaseInsensitiveContains("Voice call Duration")  ?? false {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CallInfoCell", for: indexPath) as! CallInfoCell
            cell.setData(message: message)
            return cell
        }else if identity == message.author{
            print("Reciver")
            let cell = tableView.dequeueReusableCell(withIdentifier: MessageReceiverCell.IDENTIFIER, for: indexPath) as! MessageReceiverCell
            cell.btnSelect.Indexpath = indexPath
            cell.btnSelect.addTarget(self, action: #selector(BtnSelectCellTapped), for: .touchUpInside)
            if message.hasMedia() {
                message.getMediaContentTemporaryUrl { (result, mediaContentUrl) in
                    guard let mediaContentUrl = mediaContentUrl else {
                        return
                    }
                    cell.btnSelect.ImgUrl = mediaContentUrl
                    cell.btnSelect.type = message.mediaType!
                }
            }
            cell.setData(message: message)
            return cell
        }else{
            print("Sender")
            let cell = tableView.dequeueReusableCell(withIdentifier: MessageSenderCell.IDENTIFIER, for: indexPath) as! MessageSenderCell
            cell.btnSelect.Indexpath = indexPath
            cell.btnSelect.addTarget(self, action: #selector(BtnSelectCellTapped), for: .touchUpInside)
            if message.hasMedia() {
                message.getMediaContentTemporaryUrl { (result, mediaContentUrl) in
                    guard let mediaContentUrl = mediaContentUrl else {
                        return
                    }
                    cell.btnSelect.ImgUrl = mediaContentUrl
                    cell.btnSelect.type = message.mediaType!
                }
            }
            cell.setData(message: message)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let message = self.messages[indexPath.row]
        if message.hasMedia() {
            return 200
        }else if message.body?.localizedCaseInsensitiveContains("You had a call") ?? false || message.body?.localizedCaseInsensitiveContains("Rejected call")  ?? false || message.body?.localizedCaseInsensitiveContains("Voice call Duration")  ?? false {
            return 30
        }else if message.body == "Sent you a file"{
            return 5
        }else{
            return UITableView.automaticDimension
        }
    }
}

extension ChatVC : ImagePickerDelegate{
    func didSelect(image: Any?) {
        if image != nil{
            if image is UIImage{
                let inputStream = InputStream(data: ((image! as! UIImage).pngData())!)
                QuickstartConversationsManager.shared.sendMediaMessage(self.conversation! ,inputStream) { (result, message) in
                    if ((message?.hasMedia) != nil) {
                        print("mediaFilename: \(String(describing: message?.mediaFilename)) (optional)")
                        print("mediaSize: \(String(describing: message?.mediaSize))")
                    }
                    if result.isSuccessful {
                        QuickstartConversationsManager.shared.sendMessage(self.conversation!, "Sent you a file", completion: { (result, _) in
                            self.textField.text = ""
                            self.textField.resignFirstResponder()
                        })
                    } else {
                        self.displayErrorMessage("Unable to send message")
                    }
                }
            }else if image is NSURL{
                print("Video Select.....")
                do {
                    let resources = try (image! as! NSURL).resourceValues(forKeys:[.fileSizeKey])
                    let fileSize : Int  = resources[URLResourceKey(rawValue: "NSURLFileSizeKey")] as! Int
                    print ("in KB : \( fileSize / 1000)")
                    if (fileSize / 1000) > 100000 { // 100 mb
                        print("size is to big")
                        Utils.showAlert(title: StringConstants.Message.AlertTitle.strWarning, message: StringConstants.Message.strAfterCompressLessthen100MB, viewController: self)
                        
                    }else{
                        print("upload...")
                        let path = (image as! NSURL).path!
                        let inputStream = InputStream(fileAtPath:path )!
                        QuickstartConversationsManager.shared.sendMediaMessageVideo(self.conversation!, inputStream) { (result, message) in
                            if ((message?.hasMedia) != nil) {
                                print("mediaFilename: \(String(describing: message?.mediaFilename)) (optional)")
                                print("mediaSize: \(String(describing: message?.mediaSize))")
                            }
                            if result.isSuccessful {
                                
                                QuickstartConversationsManager.shared.sendMessage(self.conversation!, "Sent you a file", completion: { (result, _) in
                                    self.textField.text = ""
                                    self.textField.resignFirstResponder()
                                })
                            } else {
                                self.displayErrorMessage("Unable to send message")
                            }
                        }
                    }
                    
                } catch {
                    print("Error: \(error)")
                }
            }
        }else{
            print("cancel by user")
        }
    }
}
// MARK: QuickstartConversationsManagerDelegate
extension ChatVC: QuickstartConversationsManagerDelegate {
    func displayStatusMessage(_ statusMessage: String) {
        self.navigationItem.prompt = statusMessage
    }
    
    func reloadMessages() {
        print("reloadMessages")
        removeSpinner()
    }
    
    // Scroll to bottom of table view for messages
    func receivedNewMessage(message : TCHMessage) {
        self.messages.append(message)
        self.conversation?.setLastReadMessageIndex(message.index!, completion: { (reslut, number) in
            print(reslut)
        })
        print(message)
        reloadConversationMessages()
    }
}
