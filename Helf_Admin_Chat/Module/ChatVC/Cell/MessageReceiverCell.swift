//
//  MessageReceiverCell.swift
//  Helf_Admin_Chat
//
//  Created by Techeniac Services on 02/09/21.
//

import UIKit

import TwilioConversationsClient
class MessageReceiverCell: UITableViewCell {
    static let IDENTIFIER = "MessageReceiverCell"
    @IBOutlet weak var lblMsg : PaddingLabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var btnimage : UIButton!
    @IBOutlet weak var imgimage : UIImageView!
    @IBOutlet weak var viewPlayimage : UIView!
    @IBOutlet weak var imgSenderTag : UIImageView!
    @IBOutlet weak var viewMedia : UIView!
    @IBOutlet weak var btnSelect: BtnTwilo!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(message: TCHMessage){
        let dateCreate = DateUtilities.convertDateToString(date: DateUtilities.convertDateFromString(dateStr: message.dateCreated!), formate: "hh:mm a")
        
        
        lblTime?.text = ""
        lblMsg?.text = ""
        lblMsg.layer.masksToBounds = true
        lblMsg.layer.cornerRadius = 10
        viewMedia.isHidden = false
        imgSenderTag.isHidden = false
        imgimage.image =  UIImage(named: "img_default")
        lblMsg.isHidden = false
        lblTime.isHidden = false

        if message.hasMedia() {
            imgSenderTag.isHidden = true
            print(message.body as Any)
            lblMsg.isHidden = true
            message.getMediaContentTemporaryUrl { [self] (result, mediaContentUrl) in
                guard let mediaContentUrl = mediaContentUrl else {
                    return
                }
                // Use the url to download an image or other media
                btnSelect.ImgUrl = mediaContentUrl
                btnSelect.type = message.mediaType!
                print(mediaContentUrl)
                viewMedia.isHidden = false
                print("Image Loding....")
                print(message.mediaType!)
                if message.mediaType == "image/jpeg" || message.mediaType == "image/png" || message.mediaType == "image/jpg"{
                    viewPlayimage.isHidden = true
                    imgimage.sd_setImage(with: URL(string: mediaContentUrl), placeholderImage:  UIImage(named: "img_default"))
                }else{
                    viewPlayimage.isHidden = false
                    imgimage.image = Utils.getThumbnailImage(forUrl: URL(string: mediaContentUrl)!)
                }
            }
            
        }else{
            viewMedia.isHidden = true
        }
        lblTime?.text = dateCreate
        lblMsg?.text = message.body
        if message.body == "Sent you a file"{
            lblMsg.isHidden = true
            lblTime.isHidden = true
            imgSenderTag.isHidden = true
        }
        lblMsg.layer.masksToBounds = true
        lblMsg.layer.cornerRadius = 10
    }
}
