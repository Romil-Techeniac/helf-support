//
//  CallInfoCell.swift
//  Helf_Admin_Chat
//
//  Created by Techeniac Services on 02/09/21.
//

import UIKit
import TwilioConversationsClient
class CallInfoCell: UITableViewCell {
    static let IDENTIFIER = "CallInfoCell"
    
    @IBOutlet weak var lblCallMsg : PaddingLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setData(message: TCHMessage){
        let dateCreate = DateUtilities.convertDateToString(date: DateUtilities.convertDateFromString(dateStr: message.dateCreated!), formate: "hh:mm a")
        if identity == message.author{
            lblCallMsg.text = "Incoming call at \(dateCreate)"
        }else{
            lblCallMsg.text = "Outgoing call at \(dateCreate)"
        }
        lblCallMsg.layer.masksToBounds = true
        lblCallMsg.layer.cornerRadius = 10
    }
}
