//
//  SplashVC.swift
//  Helf_Admin_Chat
//
//  Created by Techeniac Services on 02/09/21.
//

import UIKit
import  SwiftyUserDefaults

class SplashVC: UIViewController {
    static let IDENTIFIER = "SplashVC"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.goToNextScreen()
        })
        
    }
    func goToNextScreen(){
        if Defaults.isLoggedIn{
            let vc = ClientListVC.init(nibName: ClientListVC.IDENTIFIER, bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = LoginVC.init(nibName: LoginVC.IDENTIFIER, bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
