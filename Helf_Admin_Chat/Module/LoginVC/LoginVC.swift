//
//  LoginVC.swift
//  Helf_Admin_Chat
//
//  Created by Techeniac Services on 02/09/21.
//

import UIKit
import Alamofire
import  NVActivityIndicatorView
import SwiftyUserDefaults
class LoginVC: UIViewController {
    static let IDENTIFIER = "LoginVC"
    @IBOutlet weak var inputEmail: UITextField!
    @IBOutlet weak var inputPassword: UITextField!
    @IBOutlet weak var viewScrollView: UIScrollView!
    @IBOutlet weak var ivShowPasswordToggleButton: UIView!
    @IBOutlet weak var ivShowPasswordToggle: UIImageView!
    
    var isPasswordShowing = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)

        // Do any additional setup after loading the view.
        ivShowPasswordToggleButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(passwordToggleButtonTapped(_:))))
        #if DEBUG
        inputEmail.text = "admin@admin.com"
        inputPassword.text = "admin@123"
        #endif
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        viewScrollView.contentInset = contentInset
    }
    @objc func keyboardWillShow(notification:NSNotification) {
        guard let keyboardFrame1 = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        print(view.convert(keyboardFrame1.cgRectValue, from: nil).size.height)
        viewScrollView.contentInset.bottom = view.convert(keyboardFrame1.cgRectValue, from: nil).size.height
    }
    
    @IBAction func btnLogin_Click(){
        if isValid() {
            if !(inputEmail.text == "admin@admin.com" && inputPassword.text == "admin@123"){
                return
            }
            print("Call login api...")
            var param = Parameters()
            //        param["email"] = inputEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            //        param["password"] = inputPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            param["is_token_expiry"] = "true"
            //        param["login_type"] = "email"
            
            NetworkClient.sharedInstance.showIndicator("", stopAfter: 0)
            NetworkClient.sharedInstance.request(StringConstants.serverURL, command: "/admin/chat_token", method: .get, parameters: param) { (responce, Message) in
                print(responce,Message)
                if let responceDict = responce as? [String : Any]{
                    Defaults.isLoggedIn = true
                    UIApplication.shared.registerForRemoteNotifications()
                    Defaults.accessToken = responceDict["token"] as! String
                }
                let vc = ClientListVC.init(nibName: ClientListVC.IDENTIFIER, bundle: nil)
                self.navigationController?.pushViewController(vc, animated: true)
                
            } failure: { (error, code) in
                let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: StringConstants.Message.Button.strOk, style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    @objc func passwordToggleButtonTapped(_ : UIImageView) {
        isPasswordShowing = !isPasswordShowing
        ivShowPasswordToggle.image = isPasswordShowing ? UIImage(named: "password_unhide") : UIImage(named: "password_hide")
        inputPassword.isSecureTextEntry = !isPasswordShowing
    }
    
    func isValid() -> Bool {
        if inputEmail.text?.count == 0 || !Utils.isEmailValid(inputEmail.text!) {
            let alert = UIAlertController(title: StringConstants.Message.AlertTitle.strAlert, message: StringConstants.Message.strValidEmail, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstants.Message.Button.strOk, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        if inputPassword.text?.count == 0 {
            let alert = UIAlertController(title: StringConstants.Message.AlertTitle.strAlert, message: StringConstants.Message.strEnterPassword, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstants.Message.Button.strOk, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
}
