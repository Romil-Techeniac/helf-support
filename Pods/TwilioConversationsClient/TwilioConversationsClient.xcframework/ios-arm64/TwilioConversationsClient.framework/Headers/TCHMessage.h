//
//  TCHMessage.h
//  Twilio Conversations Client
//
//  Copyright (c) Twilio, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TCHConstants.h"
#import "TCHJsonAttributes.h"
#import "TCHAggregatedDeliveryReceipt.h"
#import "TCHDetailedDeliveryReceipt.h"

@class TCHParticipant;

/** Representation of a Message on a conversations conversation. */
__attribute__((visibility("default")))
@interface TCHMessage : NSObject

/** The unique identifier for this message. */
@property (nonatomic, copy, readonly, nullable) NSString *sid;

/** Index of Message in the Conversation's messages stream.

 By design of the conversations system the message indices may have arbitrary gaps between them,
 that does not necessarily mean they were deleted or otherwise modified - just that
 messages may have non-contiguous indices even if they are sent immediately one after another.

 Trying to use indices for some calculations is going to be unreliable.

 To calculate the number of unread messages it is better to use the read horizon API.
 See [TCHConversation getUnreadMessagesCountWithCompletion:] for details.
 */
@property (nonatomic, copy, readonly, nullable) NSNumber *index;

/** The identity of the author of the message. */
@property (nonatomic, copy, readonly, nullable) NSString *author;

/** The body of the message. */
@property (nonatomic, copy, readonly, nullable) NSString *body;

/** The type of the message. */
@property (nonatomic, assign, readonly) TCHMessageType messageType;

/** The media sid if this message has a multimedia attachment, otherwise nil. */
@property (nonatomic, copy, readonly, nullable) NSString *mediaSid;

/** The size of the attached media if present, otherwise 0. */
@property (nonatomic, assign, readonly) NSUInteger mediaSize;

/** The mime type of the attached media if present and specified at creation, otherwise nil. */
@property (nonatomic, copy, readonly, nullable) NSString *mediaType;

/** The suggested filename the attached media if present and specified at creation, otherwise nil. */
@property (nonatomic, copy, readonly, nullable) NSString *mediaFilename;

/** The SID of the participant this message is sent by. */
@property (nonatomic, copy, readonly, nullable) NSString *participantSid;

/** The participant this message is sent by. */
@property (nonatomic, copy, readonly, nullable) TCHParticipant *participant;

/** The message creation date. */
@property (nonatomic, copy, readonly, nullable) NSString *dateCreated;

/** The message creation date as an NSDate. */
@property (nonatomic, strong, readonly, nullable) NSDate *dateCreatedAsDate;

/** The message last update date. */
@property (nonatomic, copy, readonly, nullable) NSString *dateUpdated;

/** The message last update date as an NSDate. */
@property (nonatomic, strong, readonly, nullable) NSDate *dateUpdatedAsDate;

/** The identity of the user who updated the message. */
@property (nonatomic, copy, readonly, nullable) NSString *lastUpdatedBy;

/** Aggregated delivery receipt for the message. */
@property (readonly, nullable) TCHAggregatedDeliveryReceipt *aggregatedDeliveryReceipt;


/** Get detailed delivery receipts for the message.

 @param completion Completion block that will specify the result of the operation.
 */
- (void)getDetailedDeliveryReceiptsWithCompletion:(nonnull TCHDetailedDeliveryReceiptsCompletion)completion;

/** Update the body of this message
 
 @param body The new body for this message.
 @param completion Completion block that will specify the result of the operation.
 */
- (void)updateBody:(nonnull NSString *)body
        completion:(nullable TCHCompletion)completion;

/** Return this message's attributes.
 
 @return The developer-defined extensible attributes for this message.
 */
- (nullable TCHJsonAttributes *)attributes;

/** Set this message's attributes.
 
 @param attributes The new developer-defined extensible attributes for this message. (Supported types are NSString, NSNumber, NSArray, NSDictionary and NSNull)
 @param completion Completion block that will specify the result of the operation.
 */
- (void)setAttributes:(nullable TCHJsonAttributes *)attributes
           completion:(nullable TCHCompletion)completion;

/** Determine if the message has media content.
 
 @return A true boolean value if this message has media, false otherwise.
 */
- (BOOL)hasMedia;

/** Retrieve this message's media temporary link (if there is any media attached).
 This URL is impermanent, it expires in several minutes.
 If the link became invalid (expired), need to re-request the new one.
 It is user's responsibility to timely download media data by this link.

 @param completion Completion block that will specify the requested URL. If no completion block is specified, no operation will be executed.
 */
- (void)getMediaContentTemporaryUrlWithCompletion:(nonnull TCHStringCompletion)completion;

@end
